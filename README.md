# kacique - Kaeté MC :)

## Requeriments

- Docker >= 20.10.5
- Nodejs >= 12.21.0


## Backend

### Build

```
docker-compose build
```

### Run

```
docker-compose up -d
```

### Test

```
docker-compose exec app pytest
```

## frontend

### Build

```
cd frontend
npm install
```

### Test

```
npm test

```


#### Run

```
npm start
```

## Misc

### First access

```
docker-compose exec app flask shell
from backend import mongo
from werkzeug.security import generate_password_hash as gen
users = {'username': '<user>', 'password': gen('<pass>'), 'admin': True, 'status': True, 'token': ''}
mongo.db.users.insert_one(users).inserted_id

```
