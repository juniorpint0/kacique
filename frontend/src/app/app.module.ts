import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';

import { AppRoutingModule } from './app-routing.module';
import { authInterceptorProviders } from './helpers/auth.interceptor';
import { MemberModule } from './member/member.module';
import { CustomTableModule } from './shared-components/table-component/custom-table.module';
import { MotorcycleModule } from './motorcycle/motorcycle.module';
import { FinancesModule } from './finances/finances.module';
import { LoginModule } from './login/login.module';
import { DialogModule } from './shared-components/dialog-component/dialog.module';
import { SidenavModule } from './template/sidenav/sidenav.module';
import { ProfileModule } from './profile/profile.module';
import { HeaderModule } from './template/header/header.module';
import { FooterModule } from './template/footer/footer.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    SidenavModule,
    LoginModule,
    CustomTableModule,
    MemberModule,
    MotorcycleModule,
    FinancesModule,
    DialogModule,
    ProfileModule,
    HeaderModule,
    FooterModule
  ],
  exports: [],
  providers: [authInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
