import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Observable } from 'rxjs';0
import { Finance } from '../core/models/finance.model';
import { FinanceService } from '../services/finance-service/finance.service';
import { SharedService  } from '../services/shared-service/shared.service';

@Component({
  selector: 'app-finances',
  templateUrl: './finances.component.html',
  styleUrls: ['./finances.component.css']
})
export class FinancesComponent implements OnInit {
  info: any[] = [
    {
      title: 'Categoria',
      model: 'category',
      showOnTable: true
    },
    {
      title: 'Nome',
      model: 'name',
      showOnTable: true
    },
    {
      title: 'Operação',
      model: 'operation',
      showOnTable: true
    },
    {
      title: 'Método',
      model: 'method',
      showOnTable: true
    },
    {
      title: 'Data',
      model: 'date',
      customFormat: (date: Date) => {
        date = new Date(date);
        const currentDate = new Date();
        return `${date.toLocaleDateString('pt-BR')}`
      },
      showOnTable: true
    },
    {
      title: 'Valor',
      model: 'value',
      showOnTable: true
    },
    {
      title: 'Descrição',
      model: 'description',
      showOnTable: false
    },
  ];

  dataSource: MatTableDataSource<Finance>;
  dataObservable: Observable<any>;
  errorMessage = '';
  
  constructor(
    private financeService: FinanceService,
    private sharedService: SharedService
  ) { }

  ngOnInit(): void {
    this.updateDataSource();
  }

  updateDataSource() {
    this.financeService.getAll().subscribe((data: any) => {
      this.dataSource = new MatTableDataSource(data['finances']);
    }, err => {
      this.sharedService.showMessage('Erro ao carregar os dados!');
      this.errorMessage = err.error.message;
    });
  }

  deleteItem(id: any): void {
    this.financeService.delete(id).subscribe(data => {
      this.updateDataSource();
      this.sharedService.showMessage('Finança removida com sucesso!');
    })
  }
}
