import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FinancesFormComponent } from './finances-form.component';

describe('FinancesFormComponent', () => {
  let component: FinancesFormComponent;
  let fixture: ComponentFixture<FinancesFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FinancesFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FinancesFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
