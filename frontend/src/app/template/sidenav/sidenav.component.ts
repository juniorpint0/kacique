import { MediaMatcher } from '@angular/cdk/layout';
import { AfterViewInit, ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { AuthService } from 'src/app/services/auth-service/auth.service';
import { TokenService } from 'src/app/services/token-service/token.service';


@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit, AfterViewInit {

  @ViewChild(MatSidenav) sidenav: MatSidenav;

  showAdmin = false;
  username?: string;
  mobileQuery: MediaQueryList;

    fillerNav = [
      {name: "Membros", route:"member", icon:"sports_motorsports"},
      {name: "Motos", route:"motorcycle", icon:"two_wheeler"},
      {name: "Financeiro", route:"finances", icon:"savings"}
    ]

  private _mobileQueryListener: () => void;

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private media: MediaMatcher,
    private tokenService: TokenService,
    private authService: AuthService
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  shouldRun = true;

  ngOnInit(): void {
    if (this.authService.isLogged) {
      const user = this.tokenService.getUser();
      this.showAdmin =  true;
      this.username = user.username;
    }
  }

  ngAfterViewInit() {}

  isLogged(): boolean {
    return this.authService.isLogged;
  }

  logout(): void {
    this.tokenService.logout();
    window.location.reload();
  }

  getIcon(): string {
    return !this.sidenav?.opened ? 'chevron_right' : 'chevron_left';
  }
}