import { TokenService } from './../../services/token-service/token.service';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AuthService } from 'src/app/services/auth-service/auth.service';
import { SharedService } from 'src/app/services/shared-service/shared.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public id: any;

  @Output() public toggleSidenav = new EventEmitter<any>();

  constructor(
    private tokenService: TokenService,
    private authService: AuthService, //primeiro define a authServide no constructor
    private sharedService: SharedService
  ) { }

  ngOnInit(): void {
    this.id = this.sharedService.getIdFromSession();
  }

  isLogged(): boolean {
    return this.authService.isLogged; // como o html não pode acessar a variável private, precisei criar essa função que acessa o isLogged do autService
  }

  toggle(): void {
    this.toggleSidenav.emit();
  }

  logout(): void {
    this.tokenService.logout();
    this.authService.isLogged = false;
    window.location.reload();
    //colocar no onClick do botão hambúrguer (click)="snav.toggle()"
  }
}