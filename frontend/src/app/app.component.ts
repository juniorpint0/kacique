import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from './services/auth-service/auth.service';
import { TokenService } from './services/token-service/token.service';
import { SidenavComponent } from './template/sidenav/sidenav.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewInit{

  @ViewChild(SidenavComponent) sideComponent: SidenavComponent;

  title(title: any) {
    throw new Error('Method not implemented.');
  }
  showAdmin = false;
  username?: string;

  constructor(
    private tokenService: TokenService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    if (this.authService.isLogged) {
      const user = this.tokenService.getUser();
      this.showAdmin =  true;
      this.username = user.username;
    }
  }

  ngAfterViewInit() {}

  toggleSidenav(): void {
    this.sideComponent.sidenav.toggle();
  }
}