import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ComparePassword } from 'src/app/core/validators/custom.validator';
import { MemberService } from 'src/app/services/member-service/member.service';
import { SharedService } from 'src/app/services/shared-service/shared.service';

@Component({
  selector: 'app-member-form',
  templateUrl: './member-form.component.html',
  styleUrls: ['./member-form.component.css']
})
export class MemberFormComponent implements OnInit {

  public memberForm: FormGroup;
  public errorMessage: string = '';
  public disabled: boolean = false;
  public breakpoint: number;
  public colspan2: number = 2;
  public colspan3: number = 3;
  public hasData: any;
  public hidePass: boolean = true;
  public hideConfirm: boolean = true;
  public cepLoading: boolean = false;
  public usernameLoading: boolean = false;
  public timer: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: MemberService,
    private sharedService: SharedService,
    private formBuilder: FormBuilder,
    private memberService: MemberService
  ) {
    this.isEdit();
  }

  ngOnInit(): void {
    this.onResize();
  }

  findUser(event: any): void {
    const username = event.target.value;
    clearTimeout(this.timer);
    this.usernameLoading = false;
    
    if (username) {
      this.usernameLoading = true;
      this.timer = setTimeout(() => {
  
        this.memberService.getByName(username).subscribe(user => {
          const usernameControl = this.memberForm.controls.username; 
          if (user) {
            usernameControl.setErrors({ alreadyExist: true });
            usernameControl.markAsTouched();
          }
          this.usernameLoading = false;
        });
      }, 800);
    }
  }

  /**
   * Esta function checa se o form está com a rota /edit, caso true, carrega
   * os dados do membro na variável hasData.
   * @returns 
   */
  isEdit(): any {
    //Pega o id da rota
    const id = this.route.snapshot.params['id'];
    
    //Checa se os dados foram enviados pela custom-table através do navigate
    if (this.router.getCurrentNavigation()?.extras.state) {
      this.hasData = this.router.getCurrentNavigation()?.extras.state;
    }
    
    // Se não for passado pela custom-table mas existe /edit/id na rota, vai no
    // backend para consultar os dados enviados.
    if (!this.hasData && id) {
      this.service.getOne(id).subscribe(data => {
        this.hasData = data;
        this.populateForm();
      });
      return;
    }

    this.populateForm();
  }

  insertMockData() {
    this.memberForm.controls.cpf.setValue('12312312313');
    this.memberForm.controls.firstname.setValue('Daniel');
    this.memberForm.controls.lastname.setValue('Pimentel');
    this.memberForm.controls.birth.setValue(new Date());
    this.memberForm.controls.phone.setValue('82933029302');
    this.memberForm.controls.bloodType.setValue('oMinus');
    this.memberForm.controls.driverLicense.setValue('12222222323');
    this.memberForm.controls.emissionDate.setValue(new Date());
    this.memberForm.controls.expirationDate.setValue(new Date());
    this.memberForm.controls.pix.setValue('daniel@email.com');
    this.memberForm.controls.email.setValue('daniel@email.com');
    this.memberForm.controls.username.setValue('daniel');
    this.memberForm.controls.password.setValue('123456');
    this.memberForm.controls.confirmPass.setValue('123456');
    this.memberForm.controls.contact.setValue('8128128128');
    this.memberForm.controls.affiliationDate.setValue(new Date());
    this.memberForm.controls.status.setValue('active');
  }

  /**
   * Esta function cria a referência de formGroup e formControls de cada campo 
   * input já com suas regras de validação.
   */
  populateForm():void {
    this.memberForm = this.formBuilder.group({
      id: [this.hasData ? this.hasData._id : null],
      username: [this.hasData ? this.hasData.username : null,  [
        Validators.required,
        Validators.minLength(3)
      ]],
      password: [null, this.passwordValidation()],
      confirmPass: [null, this.passwordValidation()],
      firstname: [this.hasData ? this.hasData.firstname : null, [
        Validators.required, 
        Validators.minLength(3)
      ]],
      lastname: [this.hasData ? this.hasData.lastname : null, [
        Validators.required
      ]],
      cpf: [this.hasData ? this.hasData.cpf : null, [
        Validators.required,
        Validators.pattern('[0-9]{11}')
      ]],
      driverLicense: [this.hasData ? this.hasData.driverLicense : null, [
        Validators.required,
        Validators.pattern('[0-9]{11}')
      ]],
      emissionDate: [this.hasData ? this.hasData.emissionDate : null,
        Validators.required
      ],
      expirationDate: [this.hasData ? this.hasData.expirationDate : null,
        Validators.required
      ],
      pix: [this.hasData ? this.hasData.pix : null],
      email: [this.hasData ? this.hasData.email : null, [
        Validators.required,
        Validators.email
      ]],
      phone: [this.hasData ? this.hasData.phone : null, [
        Validators.required,
        Validators.pattern('[0-9]{11}')
      ]],
      contact: [this.hasData ? this.hasData.contact : null, [
        Validators.required
      ]],
      godfather: [this.hasData ? this.hasData.godfather : null],
      birth: [this.hasData ? this.hasData.birth : null, [
        Validators.required,
      ]],
      bloodType: [this.hasData ? this.hasData.bloodType : null, [
        Validators.required
      ]],
      donor: [this.hasData ? this.hasData.donor : false],
      affiliationDate: [this.hasData ? this.hasData.affiliationDate : null, [
        Validators.required
      ]],
      status: [this.hasData ? this.hasData.status : null, [
        Validators.required
      ]],
      admin: [this.hasData ? this.hasData.admin : false, [
        Validators.required
      ]],
      baptismDate: [this.hasData ? this.hasData.baptismDate : null],
      genre: [this.hasData ? this.hasData.genre : null],
      civil: [this.hasData ? this.hasData.civil : null],
      occupation: [this.hasData ? this.hasData.occupation : null],
      address: [this.hasData ? this.hasData.address : null],
      number: [this.hasData ? this.hasData.number : null],
      cep: [this.hasData ? this.hasData.cep : null, [
        Validators.pattern('[0-9]{8}')
      ]],
      neighborhood: [this.hasData ? this.hasData.neighborhood : null],
      state: [this.hasData ? this.hasData.state : null],
      city: [this.hasData ? this.hasData.city : null],
      naturalness: [this.hasData ? this.hasData.naturalness : null],
      nationality: [this.hasData ? this.hasData.nationality : null]
    }, { 
      validators: ComparePassword('password', 'confirmPass')
    });

    // this.insertMockData();
  }

  /**
   * Essa function coloca a validação dos campos de senha e confir. de senha
   * @returns 
   */
  passwordValidation() {
    if (!this.hasData) return [
      Validators.required,
      Validators.minLength(6)
    ];
    return [];
  }

  /**
   * Esta function checa se o formulário contém erros e personaliza as mensagens
   * de erro dos campos inputs
   * @param field 
   * @returns 
   */
  getErrorMessage(field: string) {
    let controls = this.memberForm.controls;

    if (controls[field].hasError('matDatepickerParse')) return 'Não é uma data válida';
    if (controls[field].hasError('mustMatch')) return 'Senhas divergentes';
    if (controls[field].hasError('required')) return 'Campo obrigatório';
    if (controls[field].hasError('email')) return 'Não é um email válido';
    if (controls[field].hasError('pattern')) return `Não é um padrão válido`;
    if (controls[field].hasError('alreadyExist')) return `Já existe um usuário com esse nome`;
    return `Mínimo de ${controls[field].errors?.minlength?.requiredLength} caracteres`;
  }

  /**
   * Essa function consulta o sharedService para consultar endereço por CEP
   */
  findAddress() {
    const cep = this.memberForm.controls.cep.value;

    if (cep && cep.length === 8) {
      this.cepLoading = true;
      this.sharedService.getAddressByCep(this.memberForm.controls.cep.value)
      .subscribe(res => {
        this.memberForm.controls.address.setValue(res.data.logradouro);
        this.memberForm.controls.neighborhood.setValue(res.data.bairro);
        this.memberForm.controls.state.setValue(res.data.uf);
        this.memberForm.controls.city.setValue(res.data.localidade);
        this.cepLoading = false;
      })
    }
  }

  /**
   * Essa function trata do redimensionamento dos campos quando muda a resolução
   * da tela
   * @param event 
   * @returns 
   */
  onResize(event?: any) {
    this.breakpoint = 1;
    this.colspan2 = 1;
    this.colspan3 = 1;

    if (window.innerWidth > 525 && window.innerWidth < 577) {
      this.breakpoint = 2;
      this.colspan2 = 2;
      this.colspan3 = 2;
      return;
    }
    if (window.innerWidth >= 577 && window.innerWidth <= 769) {
      this.breakpoint = 3;
      this.colspan2 = 2;
      this.colspan3 = 3;
      return;
    } 
    if (window.innerWidth >= 769 && window.innerWidth <= 1024) {
      this.breakpoint = 4;
      this.colspan2 = 2;
      this.colspan3 = 3;
      return;
    }
    if (window.innerWidth > 1024) {
      this.breakpoint = 5;
      this.colspan2 = 2;
      this.colspan3 = 3;
      return;
    }
  }

  /**
   * Trata do envio do formulário quando adiciona/edita um membro
   */
  onSubmit():void {
    if (this.memberForm.valid) {
      if (!this.hasData) {
        this.service.post(this.memberForm.value)
        .subscribe(() => {
          this.router.navigate(['member']);
          this.sharedService.showMessage('Usuário cadastrado com sucesso!');
        },
        (err: any) => {
          this.sharedService.showMessage('Erro ao cadastrar o usuário');
          this.errorMessage = err.error.message;
        });
      } else {
        this.service.put(
          this.memberForm.value.id.$oid,
          this.memberForm.value
        ).subscribe(() => {
          this.router.navigate(['member']);
          this.sharedService.showMessage('Usuário atualizado com sucesso!');
        },
        (err: any) => {
          this.sharedService.showMessage('Erro ao atualizar o usuário!');
          this.errorMessage = err.error.message;
        })
      }
    }
  }

  cancelar(): void{
    this.router.navigate(['member'])
  }
}