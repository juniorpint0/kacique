import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MatButtonModule } from "@angular/material/button";
import { MatDialogModule } from "@angular/material/dialog";
import { MatDividerModule } from "@angular/material/divider";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatTableModule } from "@angular/material/table";
import { CustomTableComponent } from "./custom-table.component";

@NgModule({
    imports: [
        CommonModule,
        MatIconModule,
        MatDividerModule,
        MatFormFieldModule,
        MatPaginatorModule,
        MatTableModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        MatDialogModule
    ],
    exports: [
        CustomTableComponent
    ],
    declarations: [
        CustomTableComponent
    ],
    providers: []
})

export class CustomTableModule {}