import { Injectable } from '@angular/core';
import { Router, CanActivate, 
  ActivatedRouteSnapshot,RouterStateSnapshot, UrlTree 
} from '@angular/router';
import { TokenService } from '../token-service/token.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private router: Router, 
    private tokenService: TokenService) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean|UrlTree {
      if (!this.tokenService.getToken()) {
        this.router.navigate(["login"]);
        window.location.reload();
        return false;
      }
    return true;
  }   
}