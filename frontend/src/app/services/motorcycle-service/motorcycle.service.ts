import { CustomService } from './../custom.service';
import { MotorcycleFormComponent } from '../../motorcycle/motorcycle-form/motorcycle-form.component';
import { Motorcycle } from './../../core/models/motorcycle.model';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PATH } from '../../../environments/environment';
import { catchError, map } from 'rxjs/operators';
import { TokenService } from '../token-service/token.service';

const API = `${PATH}/motorcycles/`;
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};
@Injectable({
  providedIn: 'root',
})
export class MotorcycleService {
  private token: any;
  private header: HttpHeaders;
  constructor(
    private http: HttpClient,
    private snackBar: MatSnackBar,
    private customService: CustomService,
    private tokenService: TokenService
  ) {
    this.token = this.tokenService.getUser();
    this.header = new HttpHeaders({
      Authorization: `Bearer ${this.token['token']}`,
    });
  }

  showMessage(msg: string): void {
    this.snackBar.open(msg, 'X', {
      duration: 3000,
      horizontalPosition: 'right',
      verticalPosition: 'top',
    });
  }

  get() {
    return this.http.get(API + 'get');
  }

  put(motorcycle: Motorcycle, token: any): Observable<any> {
    const header = new HttpHeaders({
      Authorization: `Bearer ${token['token']}`,
    });

    return this.http.put(API + `put/${motorcycle}`, motorcycle, {
      headers: header,
    });
  }

  /* post(owner: string, nickname: string, vehicle: string, color: string,
    year: string, plate: string, insurance: string, policy: string) {
   return this.http.post(API + 'post', {
    owner, nickname, vehicle, color, year, plate, insurance, policy
  }, httpOptions);
  } */

  read(): Observable<Motorcycle[]> {
    return this.http.get<Motorcycle[]>(API + 'get');
  }
  //ok
  readById(id: string): Observable<Motorcycle> {
    const url = `${API}get/${id}`;
    return this.http.get<Motorcycle>(url).pipe(
      map((obj) => obj),
      catchError((e) => this.customService.errorHandler(e))
    );
  }
  // ok
  create(motorcycle: Motorcycle): Observable<Motorcycle> {
    return this.http.post<Motorcycle>(API + 'post', motorcycle).pipe(
      map((obj) => obj),
      catchError((e) => this.customService.errorHandler(e))
    );
  }
  //ok
  update(id: string, motorcycle: Motorcycle): Observable<Motorcycle> {
    const url = `${API}put/${id}`;
    return this.http.put<Motorcycle>(url, motorcycle).pipe(
      map((obj) => obj),
      catchError((e) => this.customService.errorHandler(e))
    );
  }

  getOne(id: string): Observable<any> {
    return this.http.get(`${API}get/${id}`, { headers: this.header });
  }

  delete(motorcycle: any): Observable<any> {
    return this.http.delete(API + `delete/${motorcycle}`, {
      headers: this.header,
    });
  }
  //ok
  kill(id: string, motorcycle: Motorcycle): Observable<Motorcycle> {
    const url = `${API}delete/${id}`;
    return this.http.delete<any>(url).pipe(
      map((obj) => obj),
      catchError((e) => this.customService.errorHandler(e))
    );
  }
}
