import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
import { PATH } from 'src/environments/environment';
import { TokenService } from '../token-service/token.service';

const API = `${PATH}/users/`;

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  private token: any;
  private header: HttpHeaders;

  constructor(
    private http: HttpClient,
    private tokenService: TokenService,
    private snackBar: MatSnackBar
  ) {
    this.token = this.tokenService.getUser();
    this.header = new HttpHeaders({ 
      Authorization: `Bearer ${ this.token['token'] }`
    });
  }

  getAddressByCep(cep: string): Observable<any> {
    return this.http.post<any>(
      API + 'get-address',
      {'cep': cep},
      { headers: this.header }
    );
  }

  showMessage(
    msg: string, 
    dur: number = 3000
  ): void {
    this.snackBar.open(msg, 'X', {
      duration: dur,
      horizontalPosition: 'right',
      verticalPosition: 'bottom',
    });
  }

  getIdFromSession(): string | void {
    return JSON.parse(sessionStorage.user)._id;
  }
}