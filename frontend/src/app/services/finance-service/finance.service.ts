import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PATH } from '../../../environments/environment';
import { Finance } from '../../core/models/finance.model';
import { TokenService } from '../token-service/token.service';

const API = `${PATH}/finances`;

@Injectable({
  providedIn: 'root'
})
export class FinanceService {
  private token: any;
  private header: HttpHeaders;

  constructor(
    private http: HttpClient,
    private tokenService: TokenService
  ) { 
    this.token = this.tokenService.getUser(),
    this.header = new HttpHeaders({
      Authorization: `Bearer ${ this.token['token'] }`
    });
  }

  getAll(): Observable<any> {
    return this.http.get(
      `${API}/get`,
      { headers: this.header }
    );
  }

  getOne(id: string): Observable<any> {
    return this.http.get(
      `${ API }/get/${ id }`,
      { headers: this.header }
    );
  }

  post(finance: Finance): Observable<any> {
    return this.http.post<any>(
      `${ API  }/post`,
      finance,
      { headers: this.header }
    );
  }

  put(finance: Finance): Observable<any> {
    return this.http.put(
      `${ API  }/put/${ finance.id.$oid }`,
      finance, 
      { headers: this.header }
    );
  }

  delete(finance: Finance): Observable<any> {
    return this.http.delete(
      `${ API }/delete/${ finance }`,
      { headers: this.header }
    );
  }
}
