export interface Motorcycle {
    id?: string
    owner: string
    nickname: string
    vehicle: string
    color: string
    year: string
    plate: string
    insurance: string
    policy: string
}