export interface Finance {
    id: any;
    category: string;
    name: string;
    operation: string;
    method: string;
    date: string;
    value: number;
    description: string;
}
