import { ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { MatDividerModule } from '@angular/material/divider';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatIconModule } from "@angular/material/icon";

import { CustomTableModule } from '../shared-components/table-component/custom-table.module';
import { MotorcycleFormComponent } from './motorcycle-form/motorcycle-form.component';
import { MotorcycleComponent } from './motorcycle.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { NgxMaskModule } from 'ngx-mask';

@NgModule({
  declarations: [MotorcycleComponent, MotorcycleFormComponent],
  imports: [
    CommonModule,
    FormsModule,
    CustomTableModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatSnackBarModule,
    MatIconModule,
    MatGridListModule,
    MatDividerModule,
    MatSelectModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot()
  ],
  exports: [],
  providers: [],
})
export class MotorcycleModule {}
