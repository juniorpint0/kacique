import { Observable } from 'rxjs';
import { Motorcycle } from '../../core/models/motorcycle.model';
import { MotorcycleService } from '../../services/motorcycle-service/motorcycle.service';
import { SharedService } from 'src/app/services/shared-service/shared.service';
import { Component, OnInit } from '@angular/core';
import {
  ActivatedRoute,
  Router,
  UrlTree,
  RouterStateSnapshot,
} from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MemberService } from 'src/app/services/member-service/member.service';
import { Member } from 'src/app/core/models/member.model';

@Component({
  selector: 'app-motorcycle-form',
  templateUrl: './motorcycle-form.component.html',
  styleUrls: ['./motorcycle-form.component.css'],
})
export class MotorcycleFormComponent implements OnInit {
  public motorcycleForm: FormGroup;
  public errorMessage: string = '';
  public breakpoint: number;
  public colspan2: number = 2;
  public colspan3: number = 3;
  public members: any[];
  public hasData: any = null;
  public id: string;
  public title: string;
  public hidden = true;
  public currentUser: any;

  constructor(
    private motorcycleService: MotorcycleService,
    private sharedService: SharedService,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private memberService: MemberService
  ) {
    this.id = this.route.snapshot.params.id;
    if (this.id == undefined) {
      this.title = 'Nova Moto';
    } else {
      this.title = 'Informações da Moto';
      this.hidden = false;
    }
  }

  ngOnInit(): void {
    this.onResize();
    if (this.id) {
      this.motorcycleService
        .readById(this.id)
        .subscribe((motorcycle: Motorcycle) => {
          this.hasData = motorcycle;
          this.populateForm();
        });
    }
    this.getAllMembers();
    this.populateForm();
  }

  getAllMembers(): void {
    this.memberService.getAll().subscribe(data => {
      this.members = data.map((user: Member) => {
        return {
          nickname: user.username,
          name: `${user.firstname} ${user.lastname}`
        }
      });
    });
  }

  cancelar(): void {
    this.router.navigate(['motorcycle']);
  }
  /**
   * Trata do envio do formulário quando adiciona/edita um membro
   */
  createMoto(): void {
    if (this.motorcycleForm.valid) {
      if (this.id == undefined) {
        this.motorcycleService.create(this.motorcycleForm.value).subscribe(
          () => {
            this.sharedService.showMessage('Moto Criada!');
            this.router.navigate(['motorcycle']);
          },
          (err: any) => {
            this.sharedService.showMessage('Erro ao cadastrar o Moto');
            this.errorMessage = err.error.message;
          }
        );
      } else {
        this.motorcycleService
          .update(this.motorcycleForm.value.id, this.motorcycleForm.value)
          .subscribe(
            (data) => {
              this.sharedService.showMessage('Moto Atualizada!');
              this.router.navigate(['motorcycle']);
            },
            (err: any) => {
              this.sharedService.showMessage('Erro ao atualizar o Moto!');
              this.errorMessage = err.error.message;
            }
          );
      }
    }
  }

  deleteMoto(): void {
    this.motorcycleService.delete(this.id).subscribe((data) => {
      this.motorcycleService.showMessage('Moto Atualizada!');
      this.router.navigate(['motorcycle']);
    });
  }

  getLoggedOwner(): void {
    let userInfo = JSON.parse(window.sessionStorage.getItem('user') || '');
    return userInfo.username || null;
  }

  /**
   * Esta function cria a referência de formGroup e formControls de cada campo
   * input já com suas regras de validação.
   */
  populateForm(): void {
    this.motorcycleForm = this.formBuilder.group({
      id: [this.hasData ? this.id : null],
      owner: [
        this.hasData ? this.hasData.owner : this.getLoggedOwner(),
        [Validators.required]
      ],
      nickname: [
        this.hasData ? this.hasData.nickname : null,
        
      ],
      vehicle: [
        this.hasData ? this.hasData.vehicle : null,
        [Validators.required],
      ],
      color: [
        this.hasData ? this.hasData.color : null,
        [Validators.required],
      ],
      year: [
        this.hasData ? this.hasData.year : null,
        [Validators.required],
      ],
      plate: [
        this.hasData ? this.hasData.plate : null,
        [Validators.required],
      ],
      insurance: [
        this.hasData ? this.hasData.insurance : null,
        
      ],
      policy: [
        this.hasData ? this.hasData.policy : null,
        
      ],
    });
  }
  /**
   * Essa function trata do redimensionamento dos campos quando muda a resolução
   * da tela
   * @param event
   * @returns
   */
  onResize(event?: any) {
    this.breakpoint = 1;
    this.colspan2 = 1;
    this.colspan3 = 1;

    if (window.innerWidth > 525 && window.innerWidth < 577) {
      this.breakpoint = 2;
      this.colspan2 = 2;
      this.colspan3 = 2;
      return;
    }
    if (window.innerWidth >= 577 && window.innerWidth <= 769) {
      this.breakpoint = 3;
      this.colspan2 = 2;
      this.colspan3 = 3;
      return;
    }
    if (window.innerWidth >= 769 && window.innerWidth <= 1024) {
      this.breakpoint = 4;
      this.colspan2 = 2;
      this.colspan3 = 3;
      return;
    }
    if (window.innerWidth > 1024) {
      this.breakpoint = 4;
      this.colspan2 = 2;
      this.colspan3 = 3;
      return;
    }
  }
  /**
   * Esta function checa se o formulário contém erros e personaliza as mensagens
   * de erro dos campos inputs
   * @param field
   * @returns
   */
  getErrorMessage(field: string) {
    let controls = this.motorcycleForm.controls;

    if (controls[field].hasError('matDatepickerParse'))
      return 'Não é uma data válida';
    if (controls[field].hasError('mustMatch')) return 'Senhas divergentes';
    if (controls[field].hasError('required')) return 'Campo obrigatório';
    if (controls[field].hasError('email')) return 'Não é um email válido';
    if (controls[field].hasError('pattern')) return `Não é um padrão válido`;
    if (controls[field].hasError('mask')) return `Não é um padrão válido`;
    return `Mínimo de ${controls[field].errors?.minlength?.requiredLength} caracteres`;
  }
}
