import { MotorcycleService } from './../services/motorcycle-service/motorcycle.service';
import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Motorcycle } from './../core/models/motorcycle.model';
import { SharedService } from '../services/shared-service/shared.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-motorcycle',
  templateUrl: './motorcycle.component.html',
  styleUrls: ['./motorcycle.component.css'],
})
export class MotorcycleComponent implements OnInit {
  public info: any[] = [
    {
      title: 'Nome',
      model: 'owner',
      showOnTable: true,
    },
    {
      title: 'Apelido',
      model: 'nickname',
      showOnTable: true,
    },
    {
      title: 'Modelo',
      model: 'vehicle',
      showOnTable: true,
    },
    {
      title: 'Cor',
      model: 'color',
      showOnTable: true,
    },
    {
      title: 'Ano',
      model: 'year',
      showOnTable: true,
    },
    {
      title: 'Placa',
      model: 'plate',
      showOnTable: true,
      customFormat: (plate: string) => {
        let ajust;
        const parte1 = plate.slice(0, 3);
        const parte2 = plate.slice(3, 8);
        ajust = `${parte1}-${parte2}`;

        return ajust;
      },
    },
    {
      title: 'Seguro',
      model: 'insurance',
    },
    {
      title: 'Apólice',
      model: 'policy',
    },
  ];
  public dataSource: MatTableDataSource<Motorcycle>;

  constructor(
    private motorcycleService: MotorcycleService,
    private sharedService: SharedService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.updateDataSource();
  }

  deleteItem(id: any): void {
    this.motorcycleService.delete(id).subscribe((data) => {
      this.sharedService.showMessage('Moto removida com sucesso!');
      this.updateDataSource();
    });
  }
  updateDataSource() {
    this.motorcycleService.get().subscribe((data: any) => {
      this.dataSource = new MatTableDataSource(data.motorcycles);
    });
  }
}
