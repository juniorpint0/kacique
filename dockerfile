FROM debian:latest

RUN apt update
RUN apt install -y python3 python3-dev python3-pip npm

ADD . /app
WORKDIR /app

RUN pip3 install -U pip
RUN pip3 install -r backend/requirements.pip
