class Config(object):
    DEBUG = False
    TESTING = False
    SECRET_KEY = 'kacique'
    MONGO_URI = 'mongodb://db:27017/kacique'

class ProductionConfig(Config):
    MONGO_URI = 'mongodb://db:27017/kacique'

class DevelopmentConfig(Config):
    DEBUG = True

class TestingConfig(Config):
    TESTING = True
    MONGO_URI = 'mongodb://db:27017/test'
