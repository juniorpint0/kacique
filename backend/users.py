from flask import Blueprint, jsonify, request
from werkzeug.security import generate_password_hash, check_password_hash
from bson.json_util import dumps
from bson.objectid import ObjectId
from json import loads
from flask_jwt_extended import jwt_required
from . import mongo
import requests
from json import loads


bp = Blueprint('users', __name__, url_prefix='/api/users')

@bp.route('/get', methods=['GET'])
#@jwt_required
def get():
    '''Get method.'''

    if request.method == 'GET':
        try:
            queryset = loads(dumps(mongo.db.users.find()))
            return jsonify(queryset), 200
        except Exception as error:
            return jsonify(), 400

@bp.route('/get/<user_id>', methods=['GET'])
@jwt_required
def get_one(user_id):
    '''Get only one document method.'''
    if request.method == 'GET':
        try:
            queryset = loads(
                dumps(
                    mongo.db.users.find_one(
                        {'_id': ObjectId(user_id) }
                    )
                )
            )
            return jsonify(queryset), 200
        except:
            return jsonify(), 400

@bp.route('/post', methods=['POST'])
@jwt_required
def post():
    '''Post method.'''

    if request.method == 'POST':
        try:
            data = request.get_json()
            users = {
                'username': data['username'],
                'password': generate_password_hash(data['password']),
                'firstname': data['firstname'],
                'lastname': data['lastname'],
                'cpf': data['cpf'],
                'genre': data['genre'],
                'civil': data['civil'],
                'occupation': data['occupation'],
                'driverLicense': data['driverLicense'],
                'emissionDate': data['emissionDate'],
                'expirationDate': data['expirationDate'],
                'pix': data['pix'],
                'address': data['address'],
                'number': data['number'],
                'cep': data['cep'],
                'neighborhood': data['neighborhood'],
                'city': data['city'],
                'naturalness': data['naturalness'],
                'nationality': data['nationality'],
                'state': data['state'],
                'email': data['email'],
                'phone': data['phone'],
                'contact': data['contact'],
                'birth': data['birth'],
                'bloodType': data['bloodType'],
                'donor': data['donor'],
                'baptismDate': data['baptismDate'],
                'affiliationDate': data['affiliationDate'],
                'status': data['status'],
                'admin': data['admin'],
                'token': ''
            }
            mongo.db.users.insert_one(users).inserted_id
            return jsonify(), 200
        except Exception as e:
            return jsonify(), 400

@bp.route('/put/<user_id>', methods=['PUT'])
@jwt_required
def put(user_id):
    '''Put method.'''
    
    if request.method == 'PUT':
        try:
            data = request.get_json()
            user = mongo.db.users.find_one({'username': data['username']})

            if data['password'] and user['password'] != data['password']:
                data['password'] = generate_password_hash(data['password'])
            else:
                data['password'] = user['password']

            mongo.db.users.update_one(
                {'_id': ObjectId(user_id)}, 
                {'$set': data}, 
                upsert=False
            )
            return jsonify(), 200
        except Exception as error:
            return jsonify({ 'message': 'Bad request' }), 400

@bp.route('/delete/<user_id>', methods=['DELETE'])
@jwt_required
def delete(user_id):
    '''Delete method.'''
    
    if request.method == 'DELETE':
        try:
            mongo.db.users.delete_one({'_id': ObjectId(user_id)})
            return jsonify(), 200
        except:
            return jsonify(), 400

@bp.route('/get-address', methods=['POST'])
@jwt_required
def getAddress():
    '''Post method.'''

    if request.method == 'POST':
        try:
            data = request.get_json()
            req = requests.get('https://viacep.com.br/ws/' + data['cep'] + '/json')
            
            return jsonify({ 'data': loads(req.content) }), 200
        except Exception as error:
            return jsonify(), 400

@bp.route('/get-by-username/<username>', methods=['GET'])
@jwt_required
def get_by_username(username):
    '''Get only one document method.'''
    if request.method == 'GET':
        try:
            queryset = loads(
                dumps(
                    mongo.db.users.find_one(
                        { 'username': username }
                    )
                )
            )
            return jsonify(queryset), 200
        except:
            return jsonify(), 400