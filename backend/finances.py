from flask import Blueprint, jsonify, request
from bson.json_util import dumps
from bson.objectid import ObjectId
from json import loads
from flask_jwt_extended import jwt_required
from . import mongo


bp = Blueprint('finances', __name__, url_prefix='/api/finances')

@bp.route('/get', methods=['GET'])
@jwt_required
def get_all():
    '''Get method.'''

    if request.method == 'GET':
        try:
            queryset = loads(dumps(mongo.db.finances.find()))
            return jsonify({ 'finances': queryset }), 200
        except:
            return jsonify(), 400

@bp.route('/get/<finances_id>', methods=['GET'])
@jwt_required
def get_one(finances_id):
    '''Get only one document method.'''

    if request.method == 'GET':
        try:
            queryset = loads(
                dumps(
                    mongo.db.finances.find_one(
                        {'_id': ObjectId(finances_id) }
                    )
                )
            )
            return jsonify(queryset), 200
        except:
            return jsonify(), 400

@bp.route('/post', methods=['POST'])
@jwt_required
def post():
    '''Post method.'''
    
    if request.method == 'POST':
        try:
            data = request.get_json()
            finances = {
                'category': data['category'],
                'name': data['name'],
                'operation': data['operation'],
                'method': data['method'],
                'date': data['date'],
                'value': data['value'],
                'description': data['description']
            }
            mongo.db.finances.insert_one(finances).inserted_id
            return jsonify(), 200
        except:
            return jsonify(), 400

@bp.route('/put/<finances_id>', methods=['PUT'])
@jwt_required
def put(finances_id):
    '''Put method.'''

    if request.method == 'PUT':
        try:
            data = request.get_json()
            mongo.db.finances.update_one(
                {'_id': ObjectId(finances_id)},
                {
                    '$set': {
                        'category': data['category'],
                        'name': data['name'],
                        'operation': data['operation'],
                        'method': data['method'],
                        'date': data['date'],
                        'value': data['value'],
                        'description': data['description']
                    }
                }
            )
            return jsonify(), 200
        except:
            return jsonify(), 400

@bp.route('/delete/<finances_id>', methods=['DELETE'])
@jwt_required
def delete(finances_id):
    '''Delete method.'''

    if request.method == 'DELETE':
        try:
            mongo.db.finances.delete_one({'_id': ObjectId(finances_id)})
            return jsonify(), 200
        except:
            return jsonify(), 400
