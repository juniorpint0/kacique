from flask import Blueprint, jsonify, request
from werkzeug.security import generate_password_hash, check_password_hash
from bson.json_util import dumps
from bson.objectid import ObjectId
from json import loads
from flask_jwt_extended import jwt_required
from . import mongo


bp = Blueprint('motorcycles', __name__, url_prefix='/api/motorcycles')


@bp.route('/get', methods=['GET'])
@jwt_required
def get():
    '''Get method.'''

    if request.method == 'GET':
        try:
            queryset = loads(dumps(mongo.db.motorcycles.find()))
            return jsonify({ 'motorcycles': queryset  }), 200
        except:
            return jsonify(), 400

@bp.route('/get/<motorcycles_id>', methods=['GET'])
@jwt_required
def get_one(motorcycles_id):
    '''Get only one document method.'''

    if request.method == 'GET':
        try:
            queryset = loads(
                dumps(
                    mongo.db.motorcycles.find_one(
                        {'_id': ObjectId(motorcycles_id) }
                    )
                )
            )
            """ return jsonify({ 'motorcycles': queryset }), 200 """
            return jsonify(queryset), 200
        except:
            return jsonify(), 400

@bp.route('/post', methods=['POST'])
@jwt_required


def post():
    '''Post method.'''
    
    print (request.get_json())
    if request.method == 'POST':
        try:
            data = request.get_json()
            motorcycle = {
                'owner': data['owner'],
                'nickname': data['nickname'],
                'vehicle': data['vehicle'],
                'color': data['color'],
                'year': data['year'],
                'plate': data['plate'],
                'insurance': data['insurance'],
                'policy': data['policy']
                
            }
            mongo.db.motorcycles.insert_one(data).inserted_id
            return jsonify(), 200
        except:
            return jsonify(), 400

@bp.route('/put/<motorcycle_id>', methods=['PUT'])
@jwt_required
def put(motorcycle_id):
    '''Put method.'''

    if request.method == 'PUT':
        try:
            data = request.get_json()
            
            """ mongo.db.motorcycles.update_one({'_id': ObjectId(motorcycle_id)}, {'$set': data}, upsert=False) """
            """ mongo.db.motorcycles.update_one({'_id': motorcycle_id}, {'$set': data}, upsert=False) """
            mongo.db.motorcycles.update_one(
                {"_id":  ObjectId(motorcycle_id)},
                {
                    "$set": { 
                        'owner': data['owner'],
                        'nickname': data['nickname'],
                        'vehicle': data['vehicle'],
                        'color': data['color'],
                        'year': data['year'],
                        'plate': data['plate'],
                        'insurance': data['insurance'],
                        'policy': data['policy']
                    }
                } 
                
            )
            return jsonify(data), 200
        except:
            return jsonify(), 400

@bp.route('/delete/<motorcycle_id>', methods=['DELETE'])
@jwt_required
def delete(motorcycle_id):
    '''Delete method.'''
    print("AKIIIIIIII", motorcycle_id)
    if request.method == 'DELETE':
        try:
            mongo.db.motorcycles.delete_one({'_id': ObjectId(motorcycle_id)})
            return jsonify(), 200
        except:
            return jsonify(), 400
